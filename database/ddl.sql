-- Criar a tabela de clientes
CREATE TABLE client (
    id SERIAL PRIMARY KEY,
    nome VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    telefone VARCHAR(20) NOT NULL,
    latitude VARCHAR(255) NOT NULL,
    longitude VARCHAR(255) NOT NULL
);

-- Inserir alguns dados de exemplo
INSERT INTO client (nome, email, telefone, latitude, longitude)
VALUES
  ('Rafael Medeiros', 'rafael.medeiros@example.com', '+55123456789', '-23.5505', '-46.6333'),
  ('Maria Santos', 'maria.santos@example.com', '+55123456790', '-22.9068', '-43.1729'),
  ('Pedro Oliveira', 'pedro.oliveira@example.com', '+55123456791', '-27.5954', '-48.5480'),
  ('Ana Souza', 'ana.souza@example.com', '+55123456792', '-15.7942', '-47.8825'),
  ('Lucas Pereira', 'lucas.pereira@example.com', '+55123456793', '-30.0277', '-51.2287'),
  ('Juliana Costa', 'juliana.costa@example.com', '+55123456794', '-22.9035', '-43.2096'),
  ('Gabriel Martins', 'gabriel.martins@example.com', '+55123456795', '-3.7184', '-38.5423'),
  ('Mariana Lima', 'mariana.lima@example.com', '+55123456796', '-16.6864', '-49.2643'),
  ('Luiz Almeida', 'luiz.almeida@example.com', '+55123456797', '-23.5505', '-46.6333'),
  ('Carolina Araújo', 'carolina.araujo@example.com', '+55123456798', '-22.9068', '-43.1729');
