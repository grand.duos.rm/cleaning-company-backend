// client.controller.ts

import {
  Controller,
  Get,
  Post,
  Body,
  Query,
  HttpException,
  HttpStatus,
  Put,
  Param,
} from '@nestjs/common';
import { ClientService } from './client.service';
import {
  ApiTags,
  ApiOperation,
  ApiResponse,
  ApiQuery,
  ApiBody,
  ApiParam,
} from '@nestjs/swagger';
import { Client } from 'src/core/client/client.entity';
import { CreateClientDto } from './create-client.dto';
import { UpdateCoordinatesDto } from './update-coordinates.dto';
import { validate } from 'class-validator';

@ApiTags('Clientes')
@Controller('clients')
export class ClientController {
  constructor(private readonly clientService: ClientService) {}

  @ApiOperation({ summary: 'Listar todos os clientes' })
  @ApiResponse({
    status: 200,
    description: 'Lista de clientes retornada com sucesso',
    type: Client,
    isArray: true,
  })
  @ApiQuery({
    name: 'search',
    required: false,
    description: 'Termo de busca para filtrar clientes',
  })
  @Get()
  async findAll(@Query('search') search?: string): Promise<Client[]> {
    return this.clientService.findAll({ search });
  }

  @Get(':id/data')
  @ApiOperation({
    summary: 'Buscar cliente por id',
    description: 'Buscar cliente por id.',
  })
  @ApiParam({
    name: 'id',
    required: false,
    description: 'ID do cliente',
    type: Number,
  })
  @ApiResponse({
    status: 200,
    description: 'Buscar cliente por id',
    type: Client,
  })
  async getClientById(@Param('id') id: string): Promise<Client> {
    return this.clientService.getClientById(parseInt(id, 10));
  }

  @ApiOperation({ summary: 'Criar um novo cliente' })
  @ApiResponse({
    status: 201,
    description: 'Cliente criado com sucesso',
    type: Client,
  })
  @ApiBody({
    description: 'Dados do novo cliente',
    type: Client,
  })
  @Post()
  async create(@Body() createClientDto: CreateClientDto) {
    try {
      await CreateClientDto.validate(createClientDto);

      const newClient = await this.clientService.create(createClientDto);
      return newClient;
    } catch (error) {
      throw new HttpException(
        { message: 'Erro de validação', errors: error.errors },
        HttpStatus.BAD_REQUEST,
      );
    }
  }

  @Put(':id/coordinates')
  @ApiOperation({
    summary: 'Atualizar coordenadas do cliente',
    description: 'Atualiza as coordenadas X e Y de um cliente.',
  })
  @ApiParam({ name: 'id', description: 'ID do cliente', type: Number })
  @ApiBody({
    description: 'Objeto contendo as coordenadas X e Y do cliente',
    type: UpdateCoordinatesDto,
  })
  @ApiResponse({
    status: 200,
    description: 'Coordenadas atualizadas com sucesso',
  })
  @ApiResponse({
    status: 400,
    description: 'Solicitação inválida',
  })
  @ApiResponse({
    status: 404,
    description: 'Cliente não encontrado',
  })
  async updateCoordinates(
    @Param('id') id: number,
    @Body() dto: UpdateCoordinatesDto,
  ): Promise<void> {
    const validationErrors = await validate(dto); // Validação do DTO

    if (validationErrors.length > 0) {
      // Se houver erros de validação, lançar um erro 400
      throw new HttpException(
        { message: 'Solicitação inválida', errors: validationErrors },
        HttpStatus.BAD_REQUEST,
      );
    }

    try {
      await this.clientService.updateCoordinates(id, dto);
    } catch (error) {
      if (error instanceof HttpException) {
        throw error;
      } else {
        throw new HttpException(
          { message: 'Solicitação inválida' },
          HttpStatus.BAD_REQUEST,
        );
      }
    }
  }

  @Get('calculate-route')
  @ApiOperation({
    summary: 'Lista de clientes ordenada pela menor distância possível',
  })
  @ApiResponse({
    status: 200,
    description: 'Lista de clientes ordenada pela menor distância possível',
  })
  async listClientsByNearestNeighbor(): Promise<
    {
      id: number;
      nome: string;
      email: string;
      telefone: string;
      xCoordinate: any;
      yCoordinate: any;
      distance: any;
    }[]
  > {
    const { clients, distances } =
      await this.clientService.calculateNearestNeighborRoute();

    const result = clients
      .map((client, index) => ({
        id: client.id,
        nome: client.nome,
        email: client.email,
        telefone: client.telefone,
        xCoordinate: client.latitude,
        yCoordinate: client.longitude,
        distance: distances[index],
      }))
      .sort((a, b) => a.distance.value - b.distance.value);
    return result;
  }
}
