// client.service.ts

import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Client } from 'src/core/client/client.entity';
import { Repository } from 'typeorm';
import { CreateClientDto } from './create-client.dto';
import { UpdateCoordinatesDto } from './update-coordinates.dto';
import * as geolib from 'geolib';

@Injectable()
export class ClientService {
  constructor(
    @InjectRepository(Client)
    private readonly clientRepository: Repository<Client>,
  ) {}

  async findAll(filter?: { search?: string }): Promise<Client[]> {
    let sql = 'SELECT * FROM client';

    if (filter && filter.search) {
      const searchValue = `%${filter.search}%`;
      sql += ` WHERE nome ILIKE $1 OR email ILIKE $1 OR telefone ILIKE $1`;
      return this.clientRepository.query(sql, [searchValue]);
    }

    sql += ' ORDER BY id';

    return this.clientRepository.query(sql);
  }

  async getClientById(id: number): Promise<Client> {
    const sql = 'SELECT * FROM client WHERE id = $1';
    const client = await this.clientRepository.query(sql, [id]);
    return client[0];
  }

  async create(createClientDto: CreateClientDto): Promise<Client> {
    await CreateClientDto.validate(createClientDto);

    const newClient = this.clientRepository.create(createClientDto);
    return this.clientRepository.save(newClient);
  }

  async updateCoordinates(
    id: number,
    dto: UpdateCoordinatesDto,
  ): Promise<void> {
    const result = await this.clientRepository
      .createQueryBuilder()
      .update(Client)
      .set({
        latitude: dto.latitude,
        longitude: dto.longitude,
      })
      .where('id = :id', { id })
      .returning('*')
      .execute();

    if (result.affected === 0) {
      throw new NotFoundException(`Cliente com ID ${id} não encontrado`);
    }
  }

  async calculateNearestNeighborRoute(): Promise<{
    clients: Client[];
    distances: { value: number; formatted: string }[];
  }> {
    const startingPoint = { latitude: -23.494936, longitude: -47.4697381 };

    const clientsWithCoordinates = await this.clientRepository.find();

    // Ordena os clientes pelo ponto de partida
    const sortedClients = this.sortClientsByDistance(
      startingPoint,
      clientsWithCoordinates,
    );
    // Calcula as distâncias entre o ponto de partida e o primeiro cliente
    const distances: { value: number; formatted: string }[] = [
      { value: 0, formatted: '0 km' },
    ];
    const firstDistance = geolib.getDistance(
      { latitude: startingPoint.latitude, longitude: startingPoint.longitude },
      {
        latitude: sortedClients[0].latitude,
        longitude: sortedClients[0].longitude,
      },
    );
    distances.push({
      value: firstDistance / 1000,
      formatted: this.formatDistance(firstDistance / 1000),
    });

    // Calcula as distâncias entre os clientes consecutivos
    for (let i = 0; i < sortedClients.length - 1; i++) {
      const distance = geolib.getDistance(
        {
          latitude: sortedClients[i].latitude,
          longitude: sortedClients[i].longitude,
        },
        {
          latitude: sortedClients[i + 1].latitude,
          longitude: sortedClients[i + 1].longitude,
        },
      );
      // Converta a distância para quilômetros
      const distanceInKm = distance / 1000;
      distances.push({
        value: distanceInKm,
        formatted: this.formatDistance(distanceInKm),
      });
    }
    return { clients: sortedClients, distances };
  }

  private formatDistance(distanceInKm: number): string {
    return distanceInKm < 0.001
      ? 'Menos de 1 metro'
      : distanceInKm.toFixed(2) + ' km';
  }

  private sortClientsByDistance(
    startingPoint: { latitude: number; longitude: number },
    clients: Client[],
  ): Client[] {
    const sortedClients: Client[] = [];
    const unvisitedClients = [...clients]; // Copia a lista de clientes não visitados

    let currentLocation = startingPoint;

    while (unvisitedClients.length > 0) {
      let nearestNeighbor: Client | undefined;
      let minDistance = Number.MAX_SAFE_INTEGER;

      for (const unvisitedClient of unvisitedClients) {
        const distance = geolib.getDistance(
          {
            latitude: currentLocation.latitude,
            longitude: currentLocation.longitude,
          },
          {
            latitude: unvisitedClient.latitude,
            longitude: unvisitedClient.longitude,
          },
        );

        if (distance < minDistance) {
          minDistance = distance;
          nearestNeighbor = unvisitedClient;
        }
      }

      if (nearestNeighbor) {
        sortedClients.push(nearestNeighbor);
        unvisitedClients.splice(unvisitedClients.indexOf(nearestNeighbor), 1);
        currentLocation = {
          latitude: parseFloat(nearestNeighbor.latitude),
          longitude: parseFloat(nearestNeighbor.longitude),
        };
      }
    }

    return sortedClients;
  }
}
