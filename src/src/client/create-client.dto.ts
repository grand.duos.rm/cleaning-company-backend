// create-client.dto.ts

import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsEmail, IsPhoneNumber } from 'class-validator';
import * as yup from 'yup';

export class CreateClientDto {
  @ApiProperty({ example: 'John Doe', description: 'Nome do cliente' })
  @IsNotEmpty({ message: 'O campo "nome" não pode estar vazio' })
  nome: string;

  @ApiProperty({
    example: 'john.doe@example.com',
    description: 'Email do cliente',
  })
  @IsNotEmpty({ message: 'O campo "email" não pode estar vazio' })
  @IsEmail({}, { message: 'O formato do email é inválido' })
  email: string;

  @ApiProperty({
    example: '+551234567890',
    description: 'Número de telefone do cliente',
  })
  @IsNotEmpty({ message: 'O campo "telefone" não pode estar vazio' })
  @IsPhoneNumber('BR', { message: 'O formato do telefone é inválido' })
  telefone: string;

  static async validate(createClientDto: CreateClientDto): Promise<void> {
    const schema = yup.object().shape({
      nome: yup.string().required('O campo "nome" não pode estar vazio'),
      email: yup
        .string()
        .email('O formato do email é inválido')
        .required('O campo "email" não pode estar vazio'),
      telefone: yup
        .string()
        .required('O campo "telefone" não pode estar vazio'),
    });

    await schema.validate(createClientDto, { abortEarly: false });
  }
}
