import { ApiProperty } from '@nestjs/swagger';

export class UpdateCoordinatesDto {
  @ApiProperty({ example: '0.0', description: 'Nova coordenada X do cliente' })
  latitude: string;

  @ApiProperty({ example: '0.0', description: 'Nova coordenada Y do cliente' })
  longitude: string;
}
