import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClientModule } from './src/client/client.module';
import typeormConfig from './core/typeorm/typeorm.config';

@Module({
  imports: [TypeOrmModule.forRoot(typeormConfig), ClientModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
