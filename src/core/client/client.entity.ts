// client.entity.ts

import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';

@Entity()
export class Client {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'nome' })
  @ApiProperty({ example: 'John Doe', description: 'Nome do cliente' })
  nome: string;

  @Column({ name: 'email' })
  @ApiProperty({
    example: 'john.doe@example.com',
    description: 'Email do cliente',
  })
  email: string;

  @Column({ name: 'telefone' })
  @ApiProperty({
    example: '+55150000000',
    description: 'Número de telefone do cliente',
  })
  telefone: string;

  @Column({ name: 'latitude', nullable: true })
  @ApiProperty({
    example: 0.0,
    description: 'Coordenada X do cliente',
    type: 'string',
  })
  latitude: string;

  @Column({ name: 'longitude', nullable: true })
  @ApiProperty({
    example: 0.0,
    description: 'Coordenada Y do cliente',
    type: 'string',
  })
  longitude: string;
}
