# Cleaning Company - Backend

Este é o backend para o aplicativo Cleaning Company, uma plataforma para gerenciar clientes, rotas de atendimento e otimizar a visita aos clientes da empresa.

## Tecnologias Utilizadas

- **Node.js (v18):** Plataforma de execução de código JavaScript no lado do servidor.
- **NestJS:** Framework para construção de aplicativos Node.js escaláveis e eficientes.
- **Swagger:** Ferramenta para documentação de APIs.
- **TypeScript:** Superset de JavaScript que adiciona tipagem estática e outras funcionalidades.
- **PostgreSQL:** Sistema de gerenciamento de banco de dados relacional.
- **TypeORM:** Biblioteca ORM para Node.js que simplifica a interação com bancos de dados relacionais.
- **geolib:** Biblioteca JavaScript para cálculos geoespaciais, utilizada para calcular distâncias entre coordenadas geográficas.


## Instalação

1. Clone este repositório em sua máquina local:

   ```
   git clone https://gitlab.com/grand.duos.rm/cleaning-company-backend
   ```

2. Acesse o diretório do projeto:

   ```
   cd cleaning-company-backend
   ```

3. Instale as dependências:

   ```
   npm install
   ```

4. Configure as variáveis de ambiente criando um arquivo `.env` na raiz do projeto e adicionando as seguintes variáveis:

   ```
   PORT=3000
   DB_HOST=seu-host
   DB_PORT=sua-porta
   DB_USERNAME=seu-username
   DB_PASSWORD=sua-senha
   DB_DATABASE=nome-do-banco-de-dados
   DB_MIGRATION=true
   ```

5. Execute o servidor:

   ```
   npm run start
   ```

## Banco de Dados

- O arquivo de criação do banco de dados está localizado em `database/ddl.sql`.
## Abordagens e Escolhas

### TypeScript

Optou-se por utilizar TypeScript devido aos benefícios de adicionar tipagem estática ao JavaScript, o que torna o código mais legível, mantém a consistência e reduz erros durante o desenvolvimento.

### NestJS

NestJS foi escolhido como framework para construção de aplicativos Node.js devido à sua arquitetura modular, suporte para TypeScript e facilidade de integração com outros módulos e bibliotecas.

### TypeORM

TypeORM foi escolhido como ORM (Object-Relational Mapping) devido à sua compatibilidade com TypeScript e sua sintaxe intuitiva que simplifica a interação com o banco de dados PostgreSQL.

### PostgreSQL

PostgreSQL foi escolhido como banco de dados devido à sua confiabilidade, desempenho e suporte para recursos avançados, como transações ACID e suporte a tipos de dados complexos.

## Rotas da API 

A API oferece as seguintes rotas:

- `GET /clients`: Retorna todos os clientes cadastrados.
- `POST /clients`: Cria um novo cliente.
- `PUT /clients/:id/coordinates`: Atualiza as coordenadas de um cliente específico.
- `GET /clients/calculate-route`: Calcula a rota ótima para visitar os clientes.
- Outras rotas para manipulação de clientes, como consulta via id.

## Documentação

A documentação detalhada da API pode ser encontrada em `/api-docs`.

## Requisitos do Ambiente

- Node.js versão 18 ou superior
- PostgreSQL instalado e configurado localmente